import pathlib

from components.frontend import Login,Reguser, Index, Products, Contacts,User,Logout, Description, Admin, Update, Delete, LocaleTj, LocaleRu, LocaleEn

PROJECT_ROOT=pathlib.Path(__file__).parent

def setup_routes(app):
    app.router.add_get(r'/', Login.get, name='login')
    app.router.add_post(r'/', Login.post)

    app.router.add_get(r'/registration', Reguser.get, name='registration')
    app.router.add_post( r'/registration', Reguser.post)

    app.router.add_get(r'/index', Index.get,  name='index')

    app.router.add_get(r'/products', Products.get, name='products')
    app.router.add_post(r'/products', Products.post)

    app.router.add_get(r'/product/{id:\d+}', Description.get, name='description')

    app.router.add_get(r'/contacts', Contacts.get, name='contacts')
    app.router.add_post(r'/contacts', Contacts.post)


    app.router.add_get(r'/user', User.get, name='user')

# admin routes
    app.router.add_get(r'/admin', Admin.get,name='admin')
    app.router.add_post(r'/admin', Admin.post)

    app.router.add_get(r'/delete/{id:\d+}', Delete.get,name='delete')
    app.router.add_get(r'/update/{id:\d+}', Update.get, name='update')
    app.router.add_post(r'/update/{id:\d+}', Update.post)

# locale
    app.router.add_get(r'/tj', LocaleTj.get, name='tj')
    app.router.add_get(r'/ru', LocaleRu.get, name='ru')
    app.router.add_get(r'/en', LocaleEn.get, name='en')





    app.router.add_get(r'/logout',Logout.get, name='logout')


    setup_static_routes(app)


#Роути статического файлы
def setup_static_routes(app):
    app.router.add_static('/static/',
                          path=PROJECT_ROOT/'static',
                          name='static'
                          )