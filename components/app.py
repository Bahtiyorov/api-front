from aiohttp import web

from components.routes import setup_routes
from components.aio_security import setup_redis, DBAuthorizationPolicy
from .middlewares import setup_middlewares
import jinja2
import aiohttp_jinja2

from aiohttp_security import SessionIdentityPolicy
from aiohttp_security import authorized_userid
from aiohttp_security import setup as setup_security
from aiohttp_session import setup as setup_session
from aiohttp_session.redis_storage import RedisStorage


async def current_user_ctx_processor(request):
    username = await authorized_userid(request)
    #print(username)
    is_anonymous = not bool(username)
    #print('is anonymous','\t',is_anonymous)
    return {'current_user': {'is_anonymous': is_anonymous}}

async def create_app():
    app=web.Application()
    # setup jinja2 template render
    setup_middlewares(app)
    redis_pool=await setup_redis(app)
    setup_session(app, RedisStorage(redis_pool))
    aiohttp_jinja2.setup(
        app,
        loader=jinja2.PackageLoader('components', 'templates'),
        context_processors=[current_user_ctx_processor]
    )
    setup_security(app, SessionIdentityPolicy(), DBAuthorizationPolicy())

    setup_routes(app)

    return app