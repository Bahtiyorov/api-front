from aiohttp import web
import aiohttp
import json
from aiohttp_security import remember, forget, authorized_userid
from components.constants import cons, headers_cons
from components.helpers import helpers
from aiohttp_jinja2 import template
import locale
import aiosmtplib
from email.mime.text import MIMEText



def redirect(router, router_name):
    location = router[router_name].url_for()
    #print(location)
    return web.HTTPFound(location)

async def check_auth(request):
    auth_token= await authorized_userid(request)
    headers=headers_cons(auth_token)
    headers=headers.headers()
    if not auth_token:
       raise redirect(request.app.router, 'login')
    return headers






class Login(web.View):
    @template('login.html')
    async def get(self):
        #print('get')
        return {}

    @template('login.html')
    async def post(self):
        form_data=await self.post()
        if form_data['email']=='' or form_data['password']=='':
            return {'error': 'Enter data please'}
        form_data=dict(form_data)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.post(cons.uri_login, json=form_data) as response:
                data = await response.read()
                data = json.loads(data)
                # print(response.status)
                if response.status == 200:
                    resp_redirect = redirect(self.app.router, 'index')

                    if response.headers['auth_token']:
                        auth_token = response.headers['auth_token']
                        #print(auth_token)
                        await remember(self, resp_redirect, auth_token)
                        raise resp_redirect
                else:
                    # print(text['headers'])
                    return {'error': data['error']}


class Reguser(web.View):
    @template('registration.html')
    async def get(self):
        return {}

    @template('registration.html')
    async def post(self):
        form_data = await self.post()
        if form_data['email'] == '' or form_data['password'] == '' or form_data['username'] == '':
            return {'error': 'Введите данные'}
        form_data = dict(form_data)
        # print(form_data)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.post(cons.uri_reg, json=form_data) as response:
                print(response.status)
                data = await response.read()
                data = json.loads(data)
                # print(text['user'])
                if response.status == 201:
                    resp_redirect = redirect(self.app.router, 'index')
                    if response.headers['auth_token']:
                        auth_token = response.headers['auth_token']
                        # print(auth_token)
                        await remember(self, resp_redirect, auth_token)
                        raise resp_redirect
                else:
                    # print(text['headers'])
                    return {'error': data['error']}




class Index(web.View):
    @template('index.html')
    async def get(self):
        headers=await check_auth(self)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.get(cons.uri_index, headers=headers) as response:
                data=await response.read()
                data= json.loads(data)
                #print(data)
                #data=str(data)
                if response.status==200:
                    return {'data':data['images']}
                else:
                 return {}

class Products(web.View):
    @template('products.html')
    async def get(self):
        headers = await check_auth(self)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.get(cons.uri_products, headers=headers) as response:
                # print(response.status)
                data = await response.read()
                data = json.loads(data)
                #print(data['category'])
                if response.status == 200:
                    return {'data': data['products'], 'category':data['category']}
                else:
                    return {}


    @template('search.html')
    async def post(self):
        headers=await check_auth(self)
        form_data=await self.post()
        if form_data['search_key'] =='' and form_data['category'] =='':
            raise redirect(self.app.router, 'products')


        else:
            form_data=dict(form_data)
            async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
                async with session.post(cons.uri_search, headers=headers, json=form_data) as response:
                    print(response.status)
                    data = await response.read()
                    data = json.loads(data)
                    if response.status==200:
                        return {'data': data['products']}
                    elif response.status==404:
                        return {'warning':data['products']}
                    else:
                        raise redirect(self.app.router, 'products')



class Description(web.View):
    @template('description.html')
    async def get(self):
        headers = await check_auth(self)
        id=self.match_info['id']
        if id:

            uri='http://localhost:5002/desc/{}'.format(id)

            async with aiohttp.ClientSession(json_serialize=json.dumps) as session:

                async with session.get(uri, headers=headers) as response:
                    data = await response.read()
                    data = json.loads(data)
                    if response.status==200:
                        #print(data)
                        return {'product':data['product']}
                    else:
                        return {}

class Contacts(web.View):

    @template('contacts.html')
    async def get(self):
        headers = await check_auth(self)
        return {}

    @template('contacts.html')
    async def post(self):
        headers=await check_auth(self)
        post=await self.post()
        post=dict(post)

        validate=await helpers.validate_form(post)
        if validate:
            to= 'ohunjon.bakhtiyorov@gmail.com'
            password='ohunjon12081996'
            fromm=post['email']
            subject=post['firstname']
            text=post['content']
            #print(post)
            hostname='smtp.gmail.com'.encode('utf-8')
            print('0')

            try:
                print('1')
                smtp_server=aiosmtplib.SMTP()
                await smtp_server.connect(hostname='smtp.gmail.com', port=587)
                await smtp_server.login(to,password)
                print('2')
                # await smtp_server.ehlo()
                # print('3')
                # await smtp_server.starttls()
                # print('4')

                message="Subject: {}\n\n{}".format(subject,text)
                await smtp_server.sendmail(fromm,to,message)
                print('5')
                await smtp_server.close()
                print('6')
            except Exception as e:
                print(e)





        else:
            return {'error':'Fields is not be empty'}






class User(web.View):
    @template('user.html')
    async def get(self):
        headers = await check_auth(self)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.get(cons.uri_user, headers=headers) as response:
                # print(response.status)
                data = await response.read()
                data = json.loads(data)

                # print(data)
                if response.status == 200:
                    return {'user': data['user']}
                else:
                    return {}

class Logout(web.View):
    async def get(self):
        response = redirect(self.app.router, 'login')
        await forget(self, response)
        return response


class Admin(web.View):
    @template('admin.html')
    async def get(self):
        headers = await check_auth(self)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.get(cons.uri_products, headers=headers) as response:
                # print(response.status)
                data = await response.read()
                data = json.loads(data)
                # print(data['category'])
                if response.status == 200:
                    sh_index = {0: 'No', 1: 'Yes'}
                    return {'products': data['products'], 'category': data['category'], 'sh_index':sh_index}
                else:
                    return {}
    @template('admin.html')
    async def post(self):
        headers=await check_auth(self)
        post=await self.post()
        validate=await helpers.validate_form(post)
        if validate:

            uri='http://localhost:5002/add_product'
            post=dict(post)
            async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
                 async with session.post(uri, headers=headers, json=post) as response:
                    if response.status==200:
                        raise redirect(self.app.router, 'admin')

        else:
            return {'error': 'Fields must be not empty'}



class Delete(web.View):
    async def get(self):
        headers = await check_auth(self)
        id=self.match_info['id']
        if id:
            uri = 'http://localhost:5002/delete_product/{}'.format(id)

            async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
                async with session.get(uri, headers=headers) as response:
                    if response.status==200:
                        raise redirect(self.app.router,'admin')
                    else:
                        raise redirect(self.app.router, 'admin')



class Update(web.View):
    @template('update.html')
    async def get(self):
        headers = await check_auth(self)
        id=self.match_info['id']

        if id:
            uri = 'http://localhost:5002/desc/{}'.format(id)

            async with aiohttp.ClientSession(json_serialize=json.dumps) as session:

                async with session.get(uri, headers=headers) as response:
                    data = await response.read()
                    data = json.loads(data)
                    if response.status == 200:
                        #print(data)
                        sh_index={0:'No',1:'Yes'}
                        return {'product': data['product'], 'category':data['category'],'id':id, 'sh_index':sh_index}
                    else:
                        return {}

    @template('update.html')
    async def post(self):
        headers = await check_auth(self)

        form_data=await self.post()
        form_data=dict(form_data)

        validate=await helpers.validate_form(form_data)
        if validate:

            uri = 'http://localhost:5002/update_product'
            async with aiohttp.ClientSession(json_serialize=json.dumps) as session:

                async with session.post(uri, headers=headers, json=form_data) as response:
                    #print(form_data)
                    data = await response.read()
                    data = json.loads(data)
                    if response.status==200:
                        return {'success':data['result']}

        else:
            return {'error':'Fields must be not empty'}


class LocaleTj(web.View):
    async def get(self):
        lan=self.headers.get('Accept-Language')

        print(len)
        locale.setlocale(locale.LC_ALL, 'ru_RU')
        loc=locale.getlocale()
        print(loc)
        raise redirect(self.app.router, 'index')

class LocaleRu(web.View):
    async def get(self):
        locale.setlocale(locale.LC_ALL, '')
        raise redirect(self.app.router, 'index')

class LocaleEn(web.View):
    async def get(self):
        locale.setlocale(locale.LC_ALL, '')
        raise redirect(self.app.router, 'index')
