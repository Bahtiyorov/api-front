from aiohttp import web
import aiohttp
import json
from aiohttp_security import remember, forget, authorized_userid, check_authorized, is_anonymous
from components.constants import cons, headers_cons
from aiohttp_jinja2 import template



def redirect(router, router_name):
    location = router[router_name].url_for()
    #print(location)
    return web.HTTPFound(location)

async def check_auth(request):
    auth_token= await authorized_userid(request)
    headers=headers_cons(auth_token)
    headers=headers.headers()
    if not auth_token:
       raise redirect(request.app.router, 'login')
    return headers


@template('login.html')
async  def login(request):
    if request.method == 'POST':
        form_data=await request.post()
        if form_data['email'] == '' or form_data['password'] == '':
                return {'error':'Введите данные'}
        form_data=dict(form_data)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.post(cons.uri_login, json=form_data) as response:
                data = await response.read()
                data = json.loads(data)
                #print(response.status)
                if response.status==200:
                     resp_redirect=redirect(request.app.router, 'index')
                     if response.headers['auth_token']:
                         auth_token = response.headers['auth_token']
                         #print(auth_token)
                         await remember(request,resp_redirect, auth_token)
                         raise resp_redirect
                else:
                    #print(text['headers'])
                    return {'error': data['error']}


@template('registration.html')
async def registration(request):
    if request.method == 'POST':
        form_data=await request.post()
        if form_data['email'] == '' or form_data['password'] == '' or form_data['username']=='':
                return {'error':'Введите данные'}
        form_data=dict(form_data)
        #print(form_data)
        async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
            async with session.post(cons.uri_reg, json=form_data) as response:
                print(response.status)
                data=await response.read()
                data=json.loads(data)
                #print(text['user'])
                if response.status==201:
                     resp_redirect=redirect(request.app.router, 'index')
                     if response.headers['auth_token']:
                         auth_token = response.headers['auth_token']
                         #print(auth_token)
                         await remember(request,resp_redirect, auth_token)
                         raise resp_redirect
                else:
                    #print(text['headers'])
                    return {'error': data['error']}




@template('index.html')
async def index(request):
    headers=await check_auth(request)
    async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
        async with session.get(cons.uri_index, headers=headers) as response:
            data=await response.read()
            data= json.loads(data)
            data=str(data)
            if response.status==200:
                return {'data':data}
            else:
             return {}


@template('products.html')
async def products(request):
    headers = await check_auth(request)
    async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
        async with session.get(cons.uri_products, headers=headers) as response:
            # print(response.status)
            data = await response.read()
            data = json.loads(data)
            data = str(data)
            # print(data)
            if response.status == 200:
                return {'data': data}
            else:
                return {}


@template('contacts.html')
async def contacts(request):
    headers = await check_auth(request)
    async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
        async with session.get(cons.uri_products, headers=headers) as response:
            # print(response.status)
            data = await response.read()
            data = json.loads(data)
            data = str(data)
            # print(data)
            if response.status == 200:
                return {'data': data}
            else:
                return {}

@template('user.html')
async def user(request):
    headers = await check_auth(request)
    async with aiohttp.ClientSession(json_serialize=json.dumps) as session:
        async with session.get(cons.uri_products, headers=headers) as response:
            # print(response.status)
            data = await response.read()
            data = json.loads(data)
            data = str(data)
            # print(data)
            if response.status == 200:
                return {'data': data}
            else:
                return {}


async def logout(request):
    response = redirect(request.app.router, 'login')
    await forget(request, response)
    return response