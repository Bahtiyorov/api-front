

class cons:

    api_key='$2b$12$BFXHR9gxxTk5ZaT8iBkvZ.zCUuwq7cHTklNqIz35d52vmYceZT5PC'
    uri_login = 'http://127.0.0.1:5002/login'
    uri_reg = 'http://127.0.0.1:5002/registration'
    uri_index = 'http://localhost:5002/index'
    uri_products = 'http://localhost:5002/products'
    uri_search = 'http://localhost:5002/search'
    uri_contacts = 'http://localhost:5002/contacts'
    uri_user = 'http://localhost:5002/user'

class headers_cons:

    def __init__(self,auth_token):

        self.auth_token=auth_token

    def headers(self):
        #print(self.auth_token)

        headers={'Accept':'application/json','Contennt-Type':'application/json','auth_token':self.auth_token, 'api_key':cons.api_key}
        return headers
