from aiohttp_security.abc import AbstractAuthorizationPolicy
import aioredis
class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self):
        pass

    async def authorized_userid(self, identity):
        if identity is None:
            return False
        return identity

    async def permits(self, identity, permission, context=None):
        if identity is None:
            return False
        return True

async def setup_redis(app):

    pool = await aioredis.create_redis_pool((
        'localhost', 6379
    ))

    async def close_redis(app):
        print('closed pool')
        pool.close()
        await pool.wait_closed()

    app.on_cleanup.append(close_redis)
    app['redis_pool'] = pool
    return pool